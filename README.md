# OpenClassrooms Projet 2 => Intégrez un thème Wordpress pour un client

## Consignes du projet

Bravo ! Vous venez de vous lancer en tant que développeur freelance et vous avez décroché votre premier contrat.

Votre client ? L’agence immobilière “Chalets et caviar” de Courchevel.

Cette agence possède une quinzaine de chalets de luxe à la vente et une vingtaine en location.

Cependant, elle ne possède pas encore de site web pour promouvoir la vente et la location de ses chalets. C’est pour cette raison qu’elle fait appel à vous.

## Thème Wordpress

Explication du choix du thème [ici](https://gitlab.com/Boris74000/oc-projet-2-chalets-et-caviar/-/blob/master/wp-content/themes/edge_child/livrables/theme_edge.pdf)

## Documentation du thème 

[Comment utiliser le back-office](https://gitlab.com/Boris74000/oc-projet-2-chalets-et-caviar/-/blob/master/wp-content/themes/edge_child/livrables/documentation_wordpress.pdf)


